package avicTests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LoginTest extends BaseTest{
    private final static String ENTERING_LOGIN_NAME = "foundnot666@gmail.com";
    private final static String ENTERING_WRONG_PASSWORD = "12345678";
    private final static String EXPECTED_ERROR_MESSAGE = "Неверные данные авторизации.";

    @Test
    public void checkLoginWithWrongPassword(){
        getHomePage().clickOnLoginIcon();
        getLoginPage().enterLoginName(ENTERING_LOGIN_NAME);
        getLoginPage().enterPassword(ENTERING_WRONG_PASSWORD);
        getLoginPage().waitVisibilityOfElement(30, getLoginPage().getErrorMessage());
        assertTrue(getLoginPage().getErrorMessage().getText().contains(EXPECTED_ERROR_MESSAGE));
    }

}
