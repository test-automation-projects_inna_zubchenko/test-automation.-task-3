package avicTests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

public class SearchXiaomiResultsTests extends BaseTest{
    private final static String SEARCH_KEYWORD  = "Xiaomi";
    private final static int AMOUNT_OF_PRODUCTS_IS_MORE_THAN = 12;
    private final static int EXPECTED_AMOUNT_OF_PRODUCTS = 24;

    @Test
    public void checkButtonShowMore(){
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getSearchXiaomiResultsPage().scrollToButtonShowMore();
        getSearchXiaomiResultsPage().waitVisibilityOfElement(30, getSearchXiaomiResultsPage().getShowMoreButton());
        getSearchXiaomiResultsPage().clickShowMoreButton();
        getSearchXiaomiResultsPage().waitNumberOfElementsToBeMoreThan
                (30, getSearchXiaomiResultsPage().getSearchResultProducts(), AMOUNT_OF_PRODUCTS_IS_MORE_THAN);
        assertEquals(getSearchXiaomiResultsPage().getSearchResultProductsList().size(), EXPECTED_AMOUNT_OF_PRODUCTS);
    }
}
