package avicTests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

public class LaptopTests extends BaseTest{

    @Test
    public void checkDescSorting(){
        getHomePage().clickOnLaptopsAndAccessoriesButton();
        getLaptopsAndAccessoriesPage().clickLaptopButton();
        getLaptopsPage().waitVisibilityOfElement(30, getLaptopsPage().getSortingButton());
        getLaptopsPage().clickSortingButton();
        getLaptopsPage().clickDescButton();
        assertEquals(getLaptopsPage().getActualPricesList(), getLaptopsPage().getExpectedPricesList());
    }
}
