package avicTests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class RobotsCleanerTest extends BaseTest{

    private final static String FILTER_SUM = "20000";

    @Test
    public void checkPriceFilterOnPageRobotsCleaner(){
        getHomePage().moveToSmartHomeCategory();
        getHomePage().waitVisibilityOfElement(30, getHomePage().getRobotsCleanerCategory());
        getHomePage().moveAndClickRobotsCleanerCategory();
        getRobotsCleanerPage().waitVisibilityOfElement(30, getRobotsCleanerPage().getFilterForm());
        getRobotsCleanerPage().clickFilterForm();
        getRobotsCleanerPage().cleanFilterForm();
        getRobotsCleanerPage().enterFilterSum(FILTER_SUM);
        getRobotsCleanerPage().waitVisibilityOfElement(30,
                getRobotsCleanerPage().getButtonShowFilteredProducts());
        getRobotsCleanerPage().clickButtonShowFilteredProducts();
        getRobotsCleanerPage().waitVisibilityOfAllElements(30, getRobotsCleanerPage().getFilteredProducts());
        for (WebElement element:getRobotsCleanerPage().getFilteredProductsList()){
            assertTrue(Integer.parseInt(element.getText().replaceAll("\\D+",""))<Integer.parseInt(FILTER_SUM));
        }
        }
    }

