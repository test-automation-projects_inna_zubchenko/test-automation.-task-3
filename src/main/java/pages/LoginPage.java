package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage{
    public LoginPage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//div[@class = 'sign-holder clearfix']//input[@data-validate = 'login']")
    private WebElement loginInputField;

    @FindBy(xpath = "//input[@name = 'password']")
    private WebElement passwordInputField;

    @FindBy(xpath = "//div[normalize-space(text())='Неверные данные авторизации.']")
    private WebElement errorMessage;

    public void enterLoginName(final String loginName){
        loginInputField.sendKeys(loginName, Keys.ENTER);
    }
    public void enterPassword(final String password){
        passwordInputField.sendKeys(password, Keys.ENTER);
    }
    public WebElement getErrorMessage(){return errorMessage;}

}
