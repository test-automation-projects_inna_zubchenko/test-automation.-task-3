package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void waitVisibilityOfElement(long waitingTime, WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, waitingTime);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitVisibilityOfAllElements(long waitingTime, WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, waitingTime);
        wait.until(ExpectedConditions.visibilityOfAllElements(element));
    }

    public void waitNumberOfElementsToBeMoreThan(long waitingTime, By element, int numberOfElements){
        WebDriverWait wait = new WebDriverWait(driver, waitingTime);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(element, numberOfElements));
    }
}
