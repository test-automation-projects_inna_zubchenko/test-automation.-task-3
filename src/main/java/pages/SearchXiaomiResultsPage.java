package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchXiaomiResultsPage extends BasePage{
    public SearchXiaomiResultsPage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//a[contains(@class, 'show_more')]")
    private WebElement showMoreButton;


    private By searchResultProducts = By.xpath( "//div[@class='prod-cart height']");

    @FindBy(xpath = "//div[@class='prod-cart height']")
    private List<WebElement> searchResultProductsList;

    private String scrollParameters = "arguments[0].scrollIntoView();";

    public void scrollToButtonShowMore(){
        JavascriptExecutor scroller = (JavascriptExecutor) driver;
        scroller.executeScript(scrollParameters, showMoreButton);
    }
    public WebElement getShowMoreButton(){return showMoreButton;}
    public List<WebElement> getSearchResultProductsList(){return searchResultProductsList;}
    public By getSearchResultProducts(){return searchResultProducts;}
    public void clickShowMoreButton(){showMoreButton.click();}
    public int sizeOfSearchResultProductsList(){return searchResultProductsList.size();}
}
