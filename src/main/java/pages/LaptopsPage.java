package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LaptopsPage extends BasePage {
    public LaptopsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='container-main']//div[@class='sort-holder']//span[contains(@id,'select2-sort')]")
    private WebElement sortingButton;

    @FindBy(xpath = "//li[text()='По убыванию']")
    private WebElement descendingButton;

    @FindBy(xpath = "//div[@class='prod-cart__prise-new']")
    private List<WebElement> laptopListDescSorting;

    public void clickSortingButton(){sortingButton.click();}
    public void clickDescButton(){descendingButton.click();}
    public WebElement getSortingButton(){return sortingButton;}

    public List<Integer> getActualPricesList(){
        List<Integer> actualPricesList = new ArrayList<Integer>();
        for (WebElement element: laptopListDescSorting){
        actualPricesList.add(Integer.valueOf(element.getText().replaceAll("\\D+","")));
    }
        return actualPricesList;
    }
    public List<Integer> getExpectedPricesList(){
        List<Integer> expectedPricesList = getActualPricesList();
        Collections.sort(expectedPricesList);
        Collections.reverse(expectedPricesList);
        return expectedPricesList;
    }
}