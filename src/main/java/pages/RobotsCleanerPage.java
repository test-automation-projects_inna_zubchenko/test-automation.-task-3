package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class RobotsCleanerPage extends BasePage{
    public RobotsCleanerPage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath="//input[contains(@class, 'control-max')]")
    private WebElement filterForm;

    @FindBy(xpath = "//div[contains(@class,'filter-tooltip')]//span[contains(@class, 'filter')]")
    private WebElement buttonShowFilteredProducts;

    @FindBy(xpath="//div[contains(@class, 'prise-new')]")
    private WebElement filteredProducts;

    @FindBy(xpath = "//div[contains(@class, 'prise-new')]")
    private  List<WebElement> filteredProductsList;

    public WebElement getFilterForm(){return filterForm;}
    public void clickFilterForm(){filterForm.click();}
    public void cleanFilterForm(){filterForm.clear();}
    public void enterFilterSum(String filterSum){filterForm.sendKeys(filterSum, Keys.ENTER);}
    public WebElement getButtonShowFilteredProducts(){return buttonShowFilteredProducts;}
    public void clickButtonShowFilteredProducts(){buttonShowFilteredProducts.click();}
    public WebElement getFilteredProducts(){return filteredProducts;}
    public List<WebElement> getFilteredProductsList(){return filteredProductsList;}

}
