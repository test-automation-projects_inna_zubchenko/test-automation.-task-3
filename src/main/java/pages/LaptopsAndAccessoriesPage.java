package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LaptopsAndAccessoriesPage extends BasePage{
    public LaptopsAndAccessoriesPage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//div[@class='brand-box__title']//a[text()='Ноутбуки']")
    private WebElement laptopButton;

    public void clickLaptopButton(){laptopButton.click();}
}
