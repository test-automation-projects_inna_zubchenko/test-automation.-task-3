package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{
    public HomePage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//div[contains(@class, 'header-bottom')]//i[contains(@class, 'user-big')]")
    private WebElement loginIcon;

    @FindBy(xpath = "//span[text()='Ноутбуки и планшеты']")
    private WebElement laptopsAndAccessoriesButton;

    @FindBy(xpath = "//span[text()='Умный дом']")
    private WebElement smartHomeCategory;

    @FindBy(xpath = "//li[contains(@class, 'parent js')]//a[contains(@href,'roboty-pylesosy')]")
    private WebElement robotsCleanerCategory;

    @FindBy(xpath = "//input[@id='input_search']")
    private WebElement searchInput;

    public void clickOnLoginIcon(){loginIcon.click();}
    public void clickOnLaptopsAndAccessoriesButton(){laptopsAndAccessoriesButton.click();}
    public void moveToSmartHomeCategory(){
        new Actions(driver).moveToElement(smartHomeCategory).build().perform();
    }
    public void moveAndClickRobotsCleanerCategory(){
        new  Actions(driver).moveToElement(robotsCleanerCategory).click().perform();
    }
    public WebElement getRobotsCleanerCategory(){return robotsCleanerCategory;}
    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
    }

}

